
var arrA = [];
function themSo() {
  var soX = document.getElementById("so-x").value * 1;
  arrA.push(soX);
  document.getElementById("result-arr").innerText = `Mảng A gồm: ${arrA.join(", ")}`;
}

function bai1() {
  var sum = 0;
  for (var i = 0; i < arrA.length; i++) {
    if (arrA[i] > 0) {
      sum += arrA[i];
    }
  }
  document.getElementById("result-b1").innerText = `Tổng số dương: ${sum}`;
}

function bai2() {
  var cnt = 0;
  for (var i = 0; i < arrA.length; i++) {
    if (arrA[i] > 0) {
      cnt++;
    }
  }
  document.getElementById("result-b2").innerText = `Số lượng số dương: ${cnt}`;
}

function bai3() {
  var soMin = arrA[0];
  for (var i = 1; i < arrA.length; i++) {
    var current = arrA[i];
    if (current < soMin) {
      soMin = current;
    }
  }
  document.getElementById("result-b3").innerText = `Số nhỏ nhất: ${soMin}`;
}

function bai4() {
  var soDuongArr = arrA.filter(function(n) {
    return n > 0;
  });
  if (soDuongArr.length == 0) {
    document.getElementById("result-b4").innerText = `Không có số dương trong mảng`;
  }
  else {
    var soDuongMin = soDuongArr[0];
  for (var i = 1; i < soDuongArr.length; i++) {
    if (soDuongArr[i] > 0) {
      var current = soDuongArr[i];
      if (current < soDuongMin) {
        soDuongMin = current;
      }
    }
  }
  document.getElementById("result-b4").innerText = `Số dương nhỏ nhất: ${soDuongMin}`;
  }
}

function bai5() {
  var soChanArr = arrA.filter(function(n) {
    return n % 2 == 0;
  });
  if (soChanArr.length == 0) {
    document.getElementById("result-b5").innerText = `Không có số chẵn trong mảng`;
  }
  else {
    var soChanCuoiCung;
    for (var i = arrA.length - 1; i >= 0; i--) {
      if (arrA[i] % 2 == 0) {
        soChanCuoiCung = arrA[i];
        break
      }
    }
    document.getElementById("result-b5").innerText = `Số chẵn cuối cùng: ${soChanCuoiCung}`;
  }
}

function bai6() {
  var temp;
  var viTri1 = document.getElementById("vi-tri-1").value * 1;
  var viTri2 = document.getElementById("vi-tri-2").value * 1;

  var mangMoi = arrA.map(function(n) {
    return n;
  });
  if (viTri1 >= mangMoi.length || viTri1 < 0 || viTri2 >= mangMoi.length || viTri2 < 0) {
    document.getElementById("result-b6").innerText = `Vị trí nhập không hợp lệ`;
  }
  else {
    temp = mangMoi[viTri1];
    mangMoi[viTri1] = mangMoi[viTri2];
    mangMoi[viTri2] = temp;
    document.getElementById("result-b6").innerText = `Mảng sau khi đổi: ${mangMoi.join(", ")}`;
  }
}

function bai7() {
  var mangTang = arrA.sort();
  document.getElementById("result-b7").innerText = `Mảng tăng dần: ${mangTang}`;
}

function bai8() {
  var soNguyenToDauTien, check;
  var soThuongArr = [];
  for (var i = 0; i < arrA.length; i++) {
    check = true;
    if (arrA[i] < 2) {
      check = false;
    }
    else if (arrA[i] == 2) {
      check = true;
    }
    else if (arrA[i] % 2 == 0) {
      check = false;
    }
    else {
      for(var k = 3; k < Math.sqrt(arrA[i]); k += 2) {
        if (arrA[i] % k == 0) {
          check = false;
        }
      }
    }

    if (check) {
      soNguyenToDauTien = arrA[i];
      break
    }
    else {
      soThuongArr.push(arrA[i]);
    }
  }
  if (soThuongArr.length == arrA.length) {
    document.getElementById("result-b8").innerText = `Không có số nguyên tố trong mảng`;
  }
  else {
    document.getElementById("result-b8").innerText = `Số nguyên tố đầu tiên: ${soNguyenToDauTien}`;
  }
}

var arrB = [];
function themSoBai9() {
  var soN = document.getElementById("so-n").value * 1;
  arrB.push(soN);
  document.getElementById("result-arr-b9").innerText = `Mảng B gồm: ${arrB.join(", ")}`;
}

function bai9() {
  var soLuongSoNguyen = 0;
  var check;
  for(var i = 0; i < arrB.length; i++) {
    check = Number.isInteger(arrB[i]);
    if (check) {
      soLuongSoNguyen++;
    }
  }
  document.getElementById("result-b9").innerText = `Số lượng số nguyên: ${soLuongSoNguyen}`;
}

function bai10() {
  var soLuongSoDuong = soLuongSoAm = 0;
  for (var i = 0; i < arrA.length; i++) {
    if (arrA[i] > 0) {
      soLuongSoDuong++;
    }
    else if (arrA[i] < 0) {
      soLuongSoAm++;
    }
  }
  if (soLuongSoDuong > soLuongSoAm) {
    document.getElementById("result-b10").innerHTML = `<p>Số lượng số dương: ${soLuongSoDuong} - Số lượng số âm: ${soLuongSoAm}</p> <p>Số dương > Số âm</p>`;
  }
  else if (soLuongSoDuong < soLuongSoAm) {
    document.getElementById("result-b10").innerHTML = `<p>Số lượng số dương: ${soLuongSoDuong} - Số lượng số âm: ${soLuongSoAm}</p> <p>Số dương < Số âm</p>`;
  }
  else {
    document.getElementById("result-b10").innerHTML = `<p>Số lượng số dương: ${soLuongSoDuong} - Số lượng số âm: ${soLuongSoAm}</p> <p>Số dương = Số âm</p>`;
  }
}